import _ from 'lodash';
import './style.css';
import Sloth from './conan-icon.gif';

function component() {
    let element = document.createElement('div');

    element.innerHTML = _.join(['Hello', 'webpack'], ' ');
    element.classList.add('hello');

    // Add img to div.
    var img = new Image();
    img.src = Sloth;
    element.appendChild(img);

    return element;
}

document.body.appendChild(component());