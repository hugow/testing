<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- value-of calculates the string value of an XPath and inserts it into the output. -->
    <xsl:template match="person">
        <p><xsl:value-of select="name/last_name"/></p>
        <p><xsl:value-of select="hobby"/></p>
    </xsl:template>
</xsl:stylesheet>