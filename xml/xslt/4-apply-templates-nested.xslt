<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="people">
        <html>
            <head><title>Famous Scientists</title></head>
            <body>
                <!-- Tells processing to continue from child nodes of the current node. -->
                <xsl:apply-templates />
            </body>
        </html>
    </xsl:template>

    <xsl:template match="name">
        <p><xsl:value-of select="last_name"/>,
            <xsl:value-of select="first_name"/></p>
    </xsl:template>

    <xsl:template match="person">
        <xsl:apply-templates select="name"/>
    </xsl:template>

    <!-- Note that there is not <?xml> tag output here. This is because XSLT recognises when HTML is output and treats
    it as a special case. (HTML doesn't use an <?xml> tag.)-->
</xsl:stylesheet>