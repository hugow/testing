<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--
        An empty XML stylesheet will just apply all the default rules.
        Note whitespace is preserved.

        Default rules:
        1. Traverse the entire document tree. Do this by defining a rule which says match the root node and all other
        nodes and continue to apply templates for all of their children.

        Note the first node processed by the XSLT processor is the root node.

            <xsl:template match="*|/">
              <xsl:apply-templates />
            </xsl:template>

        2. Copy values of text and attribute nodes to the output document.

            <xsl:template match="text( )|@*">
              <xsl:value-of select="."/>
            </xsl:template>

        Note there are no attribute values output for this stylesheet. This is because attribute nodes aren't actually
        considered to be children of regular nodes so aren't processed in the tree traversal.

        3. Don't output anything for processing instructions or comments.

            <xsl:template match="processing-instruction()|comment( )"/>

        4. Don't copy any part of the namespace node to the output.

        There is no equivalent XSLT for this, it must be done in the source of the processor.
    -->
</xsl:stylesheet>