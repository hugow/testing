<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- When a template rule matches a node in the source document it outputs the contents of the template.
         match is an XPath. -->
    <!-- By default the XSLT processor will start processing the root node and descend down the entire tree. -->
    <xsl:template match="person">A Person</xsl:template>

    <!-- Note that after person was matched it stopped processing any children of person.
         That's why this name template doesn't match.-->
    <xsl:template match="name">Name</xsl:template>
</xsl:stylesheet>