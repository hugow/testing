<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--
        <xsl:apply-templates> lets you choose what the XSLT processor should process next.
        Its select attribute is an XPath of the nodes to process next.
        If select is blank child nodes of the current node are processed.
     -->

    <xsl:template match="person">
        <!-- Tells processing to continue from the XPath that matches name. -->
        <xsl:apply-templates select="name"/>
    </xsl:template>

    <xsl:template match="name">
        <xsl:value-of select="last_name"/>,
        <xsl:value-of select="first_name"/>
    </xsl:template>
</xsl:stylesheet>