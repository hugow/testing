<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!-- Proof that * doesn't match root. -->
    <xsl:template match="/">
        <root>
            <xsl:apply-templates />
        </root>
    </xsl:template>
    <xsl:template match="*">
        <star>
            <xsl:apply-templates />
        </star>
    </xsl:template>
</xsl:stylesheet>

<!--
See below for the difference in output when matching just * or when also matching root. Note that when matching root an
extra element is output.

XSLT:
<xsl:template match="*">
    <star>
        <xsl:apply-templates />
    </star>
</xsl:template>

Output:
<?xml version="1.0" encoding="UTF-8"?><star>
    <star>
        <star>
            <star>Alan</star>
            <star>Turing</star>
        </star>
        <star>computer scientist</star>
        <star>mathematician</star>
        <star>cryptographer</star>
    </star>
    <star>
        <star>
            <star>Richard</star>
            <star>P</star>
            <star>Feynman</star>
        </star>
        <star>physicist</star>
        <star>Playing the bongoes</star>
    </star>
</star>


XSLT:
<xsl:template match="/">
    <root>
        <xsl:apply-templates />
    </root>
</xsl:template>
<xsl:template match="*">
    <star>
        <xsl:apply-templates />
    </star>
</xsl:template>

Output:
<?xml version="1.0" encoding="UTF-8"?><root><star>
    <star>
        <star>
            <star>Alan</star>
            <star>Turing</star>
        </star>
        <star>computer scientist</star>
        <star>mathematician</star>
        <star>cryptographer</star>
    </star>
    <star>
        <star>
            <star>Richard</star>
            <star>P</star>
            <star>Feynman</star>
        </star>
        <star>physicist</star>
        <star>Playing the bongoes</star>
    </star>
</star></root>
-->