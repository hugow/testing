<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--
    Proof that:
        - * doesn't match root.
        - The node matched by the root templates is different than the root of the source XML document.
    -->
    <xsl:template match="/">
        <xsl:element name="root">
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>
    <xsl:template match="*">
        <!-- local-name gets the name of the current node. Th -->
        <xsl:element name="{local-name()}">
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>

<!--
See below for the difference in output when matching just * or when also matching root. Note that when matching root an
extra element is output.

XSLT:
<xsl:template match="*">
    <star>
        <xsl:apply-templates />
    </star>
</xsl:template>

Output:
<?xml version="1.0" encoding="UTF-8"?><star>
    <star>
        <star>
            <star>Alan</star>
            <star>Turing</star>
        </star>
        <star>computer scientist</star>
        <star>mathematician</star>
        <star>cryptographer</star>
    </star>
    <star>
        <star>
            <star>Richard</star>
            <star>P</star>
            <star>Feynman</star>
        </star>
        <star>physicist</star>
        <star>Playing the bongoes</star>
    </star>
</star>


XSLT:
<xsl:template match="/">
    <root>
        <xsl:apply-templates />
    </root>
</xsl:template>
<xsl:template match="*">
    <star>
        <xsl:apply-templates />
    </star>
</xsl:template>

Output:
<?xml version="1.0" encoding="UTF-8"?><root><star>
    <star>
        <star>
            <star>Alan</star>
            <star>Turing</star>
        </star>
        <star>computer scientist</star>
        <star>mathematician</star>
        <star>cryptographer</star>
    </star>
    <star>
        <star>
            <star>Richard</star>
            <star>P</star>
            <star>Feynman</star>
        </star>
        <star>physicist</star>
        <star>Playing the bongoes</star>
    </star>
</star></root>
-->