<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--
        The default rule which causes all nodes to be processed:

            <xsl:template match="*|/">
              <xsl:apply-templates />
            </xsl:template>

        So by defining a template for root this should override the default. As there's no apply-templates further
        processing should be prevented.
    -->
    <xsl:template match="/">Root</xsl:template>
</xsl:stylesheet>