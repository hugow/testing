<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <!--
        This XSLT tests whether a param provided at an ancestor apply-templates call will be automatically propagated
        down to child templates.

        Results:
        - For a single call to apply-templates the params are passed down as multiple nodes are processed.
        - If a new call to apply-templates in between is made then params must be manually passed down.
     -->
    <xsl:template match="people">
        <html>
            <head><title>Famous Scientists</title></head>
            <body>
                <xsl:apply-templates>
                    <xsl:with-param name="paramSeen" select="true()" />
                </xsl:apply-templates>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="person[@born='1912']">
        <div>Intermediate template matched</div>
        <!-- This proves that params are not auto passed through to a new call of apply-template. -->
        <xsl:apply-templates select="name"/>
    </xsl:template>

    <xsl:template match="name">
        <xsl:param name="paramSeen" select="false()" />
        <xsl:choose>
            <xsl:when test="$paramSeen">
                <div>Param was seen</div>
            </xsl:when>
            <xsl:otherwise>
                <div>Param was not seen</div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>