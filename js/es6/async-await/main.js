// This works with Node 10.

async function asyncFunc() {
    return 123;
}

// Async functions return a Promise.
asyncFunc().then((val) => {
    console.log(`asyncFunc().then: ${val}`);
});

// Await can only be used inside an async function.
async function awaitAsync() {
    const asyncResult = await asyncFunc();
    console.log(`awaitAsync asyncResult: ${asyncResult}`);
}
awaitAsync();

// Errors can be caught using regular try/catch.
async function throwError() {
    throw new Error('There was a problem');
}
async function awaitError() {
    try {
        const asyncResult = await throwError();
    } catch(e) {
        console.log(`Await error caught in catch statement: ${e}`);
    }
}
awaitError();