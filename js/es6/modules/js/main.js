// Default export.
import DefaultClass from './module/module-1.js';

// Not default exports must be imported with curly brackets.
import { NOT_DEFAULT } from './module/module-1.js';

// Alias.
import { NOT_DEFAULT_2 as ND_ALIAS } from './module/module-1.js';

// *.
import * as module1 from './module/module-1.js';

console.log('main');
new DefaultClass();
console.log(NOT_DEFAULT);
console.log(ND_ALIAS);

console.log('\n');
console.log(module1.NOT_DEFAULT);
console.log(module1.NOT_DEFAULT_2);
console.log(module1.default);

// Dynamic module import.
import('./module/module-1.js').then((module) => {
    console.log('\n');
    console.log(module.NOT_DEFAULT);
    console.log(module.NOT_DEFAULT_2);
});

// This doesn't work by default with Node 10, it requires a flag and mjs file extensions.