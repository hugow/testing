import React, { Component } from 'react';
import { connect } from 'react-redux'

class BasicConnectedComponent extends Component {
    render() {
        return (
            <div className="App">
                Count: {this.props.count}
                <button onClick={this.props.onIncrementClick}>Increment</button>
                <button onClick={this.props.onDecrementClick}>Decrement</button>
            </div>
        );
    }
}

// Translate the Redux store state to props for the component.
const mapStateToProps = (state) => ({
    count: state
});

// Utility for allowing components to dispatch actions to stores.
// Using object version of mapDispatchToProps. Maps keys to action creators.
const mapDispatchToProps = {
    onIncrementClick: () => ({ type: 'INCREMENT' }),
    onDecrementClick: () => ({ type: 'DECREMENT' })
};

BasicConnectedComponent = connect(
    mapStateToProps,
    mapDispatchToProps
)(BasicConnectedComponent);

export default BasicConnectedComponent;
