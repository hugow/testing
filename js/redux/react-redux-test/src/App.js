import React, { Component } from 'react';
import BasicConnectedComponent from "./BasicConnectedComponent";

class App extends Component {
  render() {
    return (
      <div className="App">
        <BasicConnectedComponent />
      </div>
    );
  }
}

export default App;
