const redux = require('redux');
const createStore = redux.createStore;
const combineReducers = redux.combineReducers;

/* Reducers */
function reducerA(state = 0, action) {
    switch(action.type) {
        case 'INCREMENT':
            return state + 1;
        default:
            return state;
    }
}
function reducerB(state = [], action) {
    switch(action.type) {
        case 'ADD_TO_ARRAY':
            return [...state, 1];
        default:
            return state;
    }
}

// Combine reducers combines other reducers. It returns a new reducer which defers
// reducing of a sub part of the state tree to each reducer. The part of the state tree
// given corresponds to the key of the object. (Using ES6 property shorthand gives the
// keys here the same names as the functions.)
const combinedReducer = combineReducers({
    reducerA,
    reducerB
});

/* Store */
const store = createStore(combinedReducer);
console.log(store.getState()); // Prints combined state of: { reducerA: 0, reducerB: [] }

store.subscribe(() => console.log(store.getState()));
store.dispatch({ type: 'INCREMENT' });
store.dispatch({ type: 'ADD_TO_ARRAY' });
store.dispatch({ type: 'ADD_TO_ARRAY' });