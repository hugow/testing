const redux = require('redux');
const createStore = redux.createStore;

/* Reducer */
// Reducers take state, action and return an updated state.
// They are pure and should return new state, not update existing state.
function reducer(state = 0, action) {
    switch(action.type) {
        case 'INCREMENT':
            return state + 1;
        case 'DECREMENT':
            return state - 1;
        default:
            return state;
    }
}

/* Store */
// Stores hold the state.
// Its API is { subscribe, dispatch, getState }.
const store = createStore(reducer);

// Stores notify when their state changes.
store.subscribe(() => console.log(store.getState()));

/* Actions */
// To update a store/state you can dispatch an action.
store.dispatch({ type: 'INCREMENT' });
store.dispatch({ type: 'INCREMENT' });
store.dispatch({ type: 'DECREMENT' });